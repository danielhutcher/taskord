<?php

namespace App\Notifications\Staff;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class VersionReleased extends Notification implements ShouldQueue
{
    use Queueable;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function via()
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'tagName' => $this->message['tagName'],
            'description' => $this->message['description'],
            'user_id' => $notifiable->id,
        ];
    }
}
