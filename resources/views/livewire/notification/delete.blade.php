<button class="btn btn-danger float-md-end mb-3" wire:click="deleteAll" wire:loading.attr="disabled">
    <x-heroicon-o-trash class="heroicon" />
    Delete all Notification
</button>
