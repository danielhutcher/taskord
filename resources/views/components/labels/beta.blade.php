<span
    class="badge-font border border-success fw-bold ms-1 px-2 rounded-pill small text-success text-capitalize"
    title="Feature Release Label: Beta"
>
    Beta
</span>
